// Copyright (C) 2019 Alexey Edelev <semlanik@gmail.com>
// SPDX-License-Identifier: LicenseRef-Qt-Commercial

#include "qtprotobuflogging_p.h"

QT_BEGIN_NAMESPACE

Q_LOGGING_CATEGORY(Protobuf, "qt.protobuf", QtWarningMsg)

QT_END_NAMESPACE
