// Copyright (C) 2022 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial

#ifndef QTPROTOBUFGLOBAL_H
#define QTPROTOBUFGLOBAL_H

#include <QtCore/qglobal.h>
#include <QtProtobuf/qtprotobufexports.h>

#endif
